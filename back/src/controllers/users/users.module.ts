import { Module } from '@nestjs/common';
import {UsersController} from "./users.controller";
import {User, UserSchema} from "../../schemas/user";
import {MongooseModule} from "@nestjs/mongoose";
import {UsersService} from "../../services/users/users.service";
import {JwtStrategyService} from "../../services/auth/jwt-strategy/jwt-strategy.service";
import {LocalStrategyService} from "../../services/auth/local-strategy/local-strategy.service";
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {jwtConstants} from "../../static/private/constants";

@Module({
    imports: [MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
        PassportModule,
        JwtModule.register({
            secret: jwtConstants.secret,
        }),
    ],
    controllers: [UsersController],
    providers: [UsersService, LocalStrategyService, JwtStrategyService]
})
export class UsersModule {}
