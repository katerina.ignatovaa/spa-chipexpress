import {Body, Controller, HttpException, HttpStatus, Param, Post, Put, UseGuards} from '@nestjs/common';
import {UsersService} from "../../services/users/users.service";
import {UserDto} from "../../dto/user-dto";
import {User} from "../../schemas/user";
import {AuthGuard} from "@nestjs/passport";

@Controller('users')
export class UsersController {

    constructor(private usersService: UsersService) {}

    @Post()
    registerUser(@Body() data: UserDto): Promise<User> {
        return this.usersService.checkUser(data.email).then((queryResult) => {
            if (queryResult.length === 0) {
                return this.usersService.sendUser(data);
            } else {
                throw new HttpException({
                    status: HttpStatus.CONFLICT,
                    errorText: 'Пользователь уже зарегистрирован'
                }, HttpStatus.CONFLICT);
            }
        });
    }

    @UseGuards(AuthGuard('local'))
    @Post(':email')
    authUser(@Body() data: UserDto, @Param('email') email) {
        return this.usersService.logIn(data)
    }

    @Put(':oldPassword')
    updateUser(@Body() data: UserDto, @Param('oldPassword') oldPassword): Promise<User>{
        if(data.email){
            return this.usersService.checkPassword(data.email, oldPassword).then((result) => {
                    return this.usersService.updateUser(data.email, data);
                }
            )
        } else {
            throw new HttpException({
                status: HttpStatus.CONFLICT,
                errorText: 'Пользователь не авторизован'
            }, HttpStatus.CONFLICT);
        }
    }
}
