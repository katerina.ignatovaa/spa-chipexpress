import { Module } from '@nestjs/common';
import { CompaniesController } from './companies.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {Company, CompanySchema} from "../../schemas/company";
import {CompanyService} from "../../services/company/company.service";

@Module({
  imports: [MongooseModule.forFeature([{ name: Company.name, schema: CompanySchema }])],
  controllers: [CompaniesController],
  providers: [CompanyService]
})
export class CompaniesModule {}
