import {Body, Controller, Get, Post} from '@nestjs/common';
import {CompanyService} from "../../services/company/company.service";
import {ICompany} from "../../interfaces/company";
import {CompanyDto} from "../../dto/company-dto";

@Controller('companies')
export class CompaniesController {

    constructor(private companyService: CompanyService) {
    }

    @Post()
    addCompany(@Body() data: CompanyDto): void{
        this.companyService.addCompany(data)
    }

    @Get()
    getCompanies(): Promise<ICompany[]>{
        return this.companyService.getCompanies();
    }
}
