import { Module } from '@nestjs/common';
import { ProductsController } from './products.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {Product, ProductSchema} from "../../schemas/product";
import {ProductService} from "../../services/product/product.service";

@Module({
  imports: [MongooseModule.forFeature([{ name: Product.name, schema: ProductSchema }])],
  controllers: [ProductsController],
  providers: [ProductService]
})
export class ProductsModule {}
