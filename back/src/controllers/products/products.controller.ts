import {Body, Controller, Delete, Get, Param, Post, Put, UseInterceptors} from '@nestjs/common';
import {ProductService} from "../../services/product/product.service";
import {Product} from "../../schemas/product";
import {FileInterceptor} from "@nestjs/platform-express";
import {diskStorage} from "multer";
import {ProductDto} from "../../dto/product-dto";

@Controller('products')
export class ProductsController {

    static fileName: string;

    constructor(private productService: ProductService) {}

    @Get()
    getProducts(): Promise<Product[]>{
       return this.productService.getProducts()
    }

    @Post(':company')
    getProductsByCompany(@Param('company') company): Promise<Product[]>{
        return this.productService.getProductsByCompany(company)
    }

    @Post()
    @UseInterceptors(FileInterceptor('img', {
        storage: diskStorage({
            destination: './public/',
            filename: (req, file, callback) => {
                const imgType = file.mimetype.split('/');
                const uniqueCode = Date.now();
                const fileName = file.fieldname + '-' + uniqueCode + '.' + imgType[1];
                callback(null, fileName);
                ProductsController.fileName = fileName;
            }
        })
    }))
    addProduct(@Body() data: ProductDto): Promise<Product[]>{
        data.img = ProductsController.fileName;
        ProductsController.fileName = '';
        return this.productService.addProduct(data);
    }

    @Get(':id')
    getProductById(@Param('id') id): Promise<Product>{
        return this.productService.getProductById(id)
    }

    @Put(':id')
    @UseInterceptors(FileInterceptor('img', {
        storage: diskStorage({
            destination: './public/',
            filename: (req, file, callback) => {
                const imgType = file.mimetype.split('/');
                const uniqueCode = Date.now();
                const fileName = file.fieldname + '-' + uniqueCode + '.' + imgType[1];
                callback(null, fileName);
                ProductsController.fileName = fileName;
            }
        })
    }))
    updateProduct(@Body() data: ProductDto, @Param('id') id): Promise<Product[]>{
        data.img = ProductsController.fileName;
        ProductsController.fileName = '';
        return this.productService.updateProduct(id, data);
    }

    @Delete(':id')
    deleteProductById(@Param('id') id): Promise<Product[]>{
        return this.productService.deleteProductById(id)
    }
}
