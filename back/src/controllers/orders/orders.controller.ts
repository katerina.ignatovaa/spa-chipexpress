import {Body, Controller, Delete, Get, Param, Post, UseGuards} from '@nestjs/common';
import {OrdersService} from "../../services/orders/orders.service";
import {OrderDto} from "../../dto/order-dto";
import {Order} from "../../schemas/order";
import {AuthGuard} from "@nestjs/passport";

@Controller('orders')
export class OrdersController {

    constructor(private orderService: OrdersService) {}

    @UseGuards(AuthGuard('jwt'))
    @Post()
    addOrder(@Body() data: OrderDto): void{
        return this.orderService.addOrder(data);
    }

    @Get()
    getOrders(): Promise<Order[]>{
        return this.orderService.getOrders()
    }

    @Get(':email')
    getOrderForUser(@Param('email') email): Promise<Order[]>{
        return this.orderService.getOrderForUser(email)
    }

    @Delete(':id')
    deleteOrder(@Param('id') id): Promise<Order[]>{
        return this.orderService.deleteOrder(id)
    }
}
