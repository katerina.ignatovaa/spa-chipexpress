export interface IOrder{
    productName: string,
    price: string,
    userName: string,
    email: string,
    phone: string,
    date: string,
    orderNumber: string
}