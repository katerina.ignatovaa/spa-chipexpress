import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {UsersModule} from "./controllers/users/users.module";
import { MongooseModule } from '@nestjs/mongoose';
import {ProductsModule} from "./controllers/products/products.module";
import {CompaniesModule} from "./controllers/companies/companies.module";
import {OrdersModule} from "./controllers/orders/orders.module";

@Module({
  imports: [UsersModule, ProductsModule, CompaniesModule, OrdersModule, MongooseModule.forRoot('mongodb://127.0.0.1:27017/chipexpress')],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
