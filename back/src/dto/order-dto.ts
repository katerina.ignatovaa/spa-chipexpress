import {IOrder} from "../interfaces/order";

export class OrderDto implements IOrder {
    productName: string;
    price: string;
    userName: string;
    email: string;
    phone: string;
    date: string;
    orderNumber: string
}
