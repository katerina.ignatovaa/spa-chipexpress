import {IProduct} from "../interfaces/product";

export class ProductDto implements IProduct {
    name: string;
    company: string;
    description: string;
    price: string;
    img: string
}