import {ICompany} from "../interfaces/company";

export class CompanyDto implements ICompany {
    name: string;
}