import {IUser} from "../interfaces/user";

export class UserDto implements IUser {
    email: string;
    password: string;
    key?: string
}