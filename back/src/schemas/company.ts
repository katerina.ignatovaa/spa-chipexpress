import {HydratedDocument} from "mongoose";
import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";
import {ICompany} from "../interfaces/company";

export type CompanyDocument = HydratedDocument<Company>;

@Schema()
export class Company implements ICompany {
    @Prop() name: string;
}

export const CompanySchema = SchemaFactory.createForClass(Company);