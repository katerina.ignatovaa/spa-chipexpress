import {HydratedDocument} from "mongoose";
import {Prop, Schema, SchemaFactory} from "@nestjs/mongoose";
import {IOrder} from "../interfaces/order";

export type OrderDocument = HydratedDocument<Order>;

@Schema()
export class Order implements IOrder {
     @Prop() productName: string;
     @Prop() price: string;
     @Prop() userName: string;
     @Prop() email: string;
     @Prop() phone: string;
     @Prop() date: string;
     @Prop() orderNumber: string;
}

export const OrderSchema = SchemaFactory.createForClass(Order);