import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import {IUser} from "../interfaces/user";

export type UserDocument = HydratedDocument<User>;

@Schema()
export class User implements IUser {
    @Prop() email: string;
    @Prop() password: string;
    @Prop() key?: string;
}

export const UserSchema = SchemaFactory.createForClass(User);