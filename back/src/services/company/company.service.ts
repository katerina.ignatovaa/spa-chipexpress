import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Company, CompanyDocument} from "../../schemas/company";
import {ICompany} from "../../interfaces/company";
import {CompanyDto} from "../../dto/company-dto";

@Injectable()
export class CompanyService {

    constructor(@InjectModel(Company.name) private companyModel: Model<CompanyDocument>) {}

    addCompany(data: CompanyDto): void{
        const companyData = new this.companyModel(data);
        companyData.save();
    }

    async getCompanies(): Promise<ICompany[]>{
        return this.companyModel.find()
    }
}
