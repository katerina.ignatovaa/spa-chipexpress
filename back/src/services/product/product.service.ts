import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Product, ProductDocument} from "../../schemas/product";
import {ProductDto} from "../../dto/product-dto";

@Injectable()
export class ProductService {

    constructor(@InjectModel(Product.name) private productModel: Model<ProductDocument>) {}

    async getProducts(): Promise<Product[]>{
        return this.productModel.find()
    }

    async getProductsByCompany(company: string): Promise<Product[]>{
        return this.productModel.find({company: company})
    }

    async addProduct(data: ProductDto): Promise<Product[]>{
        const productData = new this.productModel(data);
        await productData.save();
        return this.productModel.find()
    }

    async getProductById(id: string): Promise<Product>{
        return this.productModel.findById(id)
    }

    async updateProduct(id: string, data: ProductDto): Promise<any>{
        await this.productModel.findByIdAndUpdate(id, data);
        return this.productModel.find()
    }

    async deleteProductById(id: string): Promise<Product[]>{
        await this.productModel.findByIdAndRemove(id);
        return this.productModel.find()
    }
}
