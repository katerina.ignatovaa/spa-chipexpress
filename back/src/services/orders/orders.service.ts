import { Injectable } from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Order, OrderDocument} from "../../schemas/order";
import {OrderDto} from "../../dto/order-dto";

@Injectable()
export class OrdersService {

    constructor(@InjectModel(Order.name) private orderModel: Model<OrderDocument>) {}

    addOrder(data: OrderDto): void{
        const orderData = new this.orderModel(data);
        orderData.save();
    }

    async getOrders(): Promise<Order[]>{
        return this.orderModel.find()
    }

    async getOrderForUser(email: string): Promise<Order[]>{
        return this.orderModel.find({email: email})
    }

    async deleteOrder(id: string): Promise<Order[]>{
        const order = await this.orderModel.findById(id);
        const email = order.email;
        await this.orderModel.findByIdAndRemove(id);
        return this.orderModel.find({email: email})
    }
}
