import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {User, UserDocument} from "../../schemas/user";
import {Model} from "mongoose";
import {UserDto} from "../../dto/user-dto";
import {JwtService} from "@nestjs/jwt";
import {accessKey} from "../../static/private/constants";
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {

    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>,
                private jwtService: JwtService) {}

    async checkUser(email: string): Promise<User[]> {
        return this.userModel.find({email: email});
    }

    async sendUser(data: UserDto): Promise<User>{
        const salt = await bcrypt.genSalt();
        const hash = await bcrypt.hash(data.password, salt);
        data.password = hash;
        const userData = new this.userModel(data);
        return userData.save();
    }

    async logIn(data: UserDto) {
        const payload = {email: data.email, password: data.password};
        const isAdmin = data.key === accessKey;
        return {
            access_token: this.jwtService.sign(payload),
            isUserAdmin: isAdmin
        }
    }

    async checkPassword(email: string, oldPassword: string): Promise<User[]> {
        const user = await this.userModel.find({email: email});
        if(user.length !== 0){
            const isMatch = await bcrypt.compare(oldPassword, user[0].password);
            if(isMatch){
                return user
            } else {
                throw new HttpException({
                    status: HttpStatus.CONFLICT,
                    errorText: 'Неверно введен старый пароль'
                }, HttpStatus.CONFLICT);
            }
        }
    }

    async updateUser(email: string, data: UserDto): Promise<User>{
        const salt = await bcrypt.genSalt();
        const hash = await bcrypt.hash(data.password, salt);
        data.password = hash;
        return this.userModel.findOneAndUpdate({email: email}, data)
    }
}
