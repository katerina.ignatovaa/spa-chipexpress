import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {PassportStrategy} from "@nestjs/passport";
import {Strategy} from "passport-local";
import {UsersService} from "../../users/users.service";
import * as bcrypt from 'bcrypt';

@Injectable()
export class LocalStrategyService extends PassportStrategy(Strategy){

    constructor(private usersService: UsersService) {
        super({usernameField: 'email'});
    }

    async validate(email: string, password: string): Promise<any>{
        const user = await this.usersService.checkUser(email);
        if(user.length === 0){
            throw new HttpException({
                status: HttpStatus.CONFLICT,
                errorText: 'Пользователь не зарегистрирован'
            }, HttpStatus.CONFLICT);
        } else {
            const isMatch = await bcrypt.compare(password, user[0].password);
            if(isMatch){
                return true;
            } else {
                throw new HttpException({
                    status: HttpStatus.CONFLICT,
                    errorText: 'Неверный пароль'
                }, HttpStatus.CONFLICT);
            }
        }
    }
}
