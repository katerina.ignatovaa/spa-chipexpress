export interface IProduct{
  name: string,
  company: string,
  description: string,
  price: string,
  img: string,
  _id?: string
}

