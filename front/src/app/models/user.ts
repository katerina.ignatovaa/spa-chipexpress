export interface IUser{
  email: string,
  password: string,
  key?: string,
  _id?: string
}
