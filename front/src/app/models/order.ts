export interface IOrderProduct{
  productName: string,
  company: string,
  price: string,
  img: string,
}

export interface IOrder{
  productName: string,
  price: string,
  userName: string,
  email: string,
  phone: string,
  date: string,
  orderNumber: string,
  _id?: string
}
