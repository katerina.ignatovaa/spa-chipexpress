import { Injectable } from '@angular/core';
import {IOrder, IOrderProduct} from "../../models/order";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserService} from "../user/user.service";
import {IUser} from "../../models/user";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  orders: IOrderProduct[] = [];
  ordersCopy: IOrderProduct[];

  constructor(private http: HttpClient,
              private userService: UserService) {}

  addOrder(order: IOrderProduct, user: IUser){
    if(!this.orders[0]) {
      this.orders = window.localStorage.getItem(user.email +'_orders') ? JSON.parse(window.localStorage.getItem(user.email +'_orders') as string) : [];
    }
    console.log(this.orders)
    this.orders.push(order);
    this.ordersCopy = [...this.orders];
    window.localStorage.setItem(user.email +'_orders', JSON.stringify(this.orders))
  }

  getOrders(email: string){
    this.orders = window.localStorage.getItem(email +'_orders') ? JSON.parse(window.localStorage.getItem(email +'_orders') as string) : [];
    this.ordersCopy = [...this.orders];
    return this.orders;
  }

  deleteOrder(index: number, email: string){
    this.ordersCopy.splice(index,1);
    this.orders = this.ordersCopy;
    this.ordersCopy = [...this.orders];
    window.localStorage.setItem(email +'_orders', JSON.stringify(this.orders))
  }

  deleteAll(email: string){
    this.orders = [];
    this.ordersCopy = [];
    window.localStorage.removeItem(email +'_orders');
  }

  sendOrder(order: IOrder){
    return this.http.post('http://localhost:3000/orders/', order);
  }

  getOrdersfromDb(): Observable<IOrder[]>{
    return this.http.get<IOrder[]>('http://localhost:3000/orders/');
  }

  getOrderForUser(): Observable<IOrder[]>{
    const user = this.userService.getUser();
    return this.http.get<IOrder[]>('http://localhost:3000/orders/'+ user.email)
  }

  deleteOrderFromDb(order:IOrder): Observable<IOrder[]>{
    return this.http.delete<IOrder[]>('http://localhost:3000/orders/'+ order._id);
  }
}
