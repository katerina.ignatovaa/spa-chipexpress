import { Injectable } from '@angular/core';
import {IUser} from "../../models/user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  setUser(user: IUser){
    window.localStorage.setItem('user', JSON.stringify(user))
  }

  getUser(){
    return JSON.parse(window.localStorage.getItem('user') as string);
  }

  setToken(token: string){
    window.localStorage.setItem('token', JSON.stringify(token))
  }

  getToken(){
    return JSON.parse(window.localStorage.getItem('token') as string);
  }

  setAdmin(isUserAdmin: boolean){
    window.localStorage.setItem('admin', JSON.stringify(isUserAdmin))
  }

  getAdmin(){
    return JSON.parse(window.localStorage.getItem('admin') as string);
  }
}
