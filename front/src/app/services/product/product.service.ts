import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IProduct} from "../../models/product";
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private productSubject = new Subject<IProduct[]>();
  readonly productSubject$ = this.productSubject.asObservable();
  static productIdForUpdate: string;
  selectedProduct: IProduct;

  constructor(private http: HttpClient) { }

  getProducts(): Observable<IProduct[]>{
    return this.http.get<IProduct[]>('http://localhost:3000/products/');
  }

  getProductsByCompany(company: string): Observable<IProduct[]>{
    return this.http.post<IProduct[]>('http://localhost:3000/products/'+ company, {});
  }

  addProduct(data: any): Observable<IProduct[]>{
    return this.http.post<IProduct[]>('http://localhost:3000/products/', data);
  }

  getProductById(id: string): Observable<IProduct>{
    return this.http.get<IProduct>('http://localhost:3000/products/'+ id);
  }

  updateProductsList(data: any) {
    this.productSubject.next(data)
}

  updateProduct(data: any): Observable<IProduct[]>{
    return this.http.put<IProduct[]>('http://localhost:3000/products/'+ ProductService.productIdForUpdate, data);
  }

  deleteProductById(id: string): Observable<IProduct[]>{
    return this.http.delete<IProduct[]>('http://localhost:3000/products/'+ id);
  }

  setSelectedProduct(product: IProduct){
    this.selectedProduct = product;
  }

  getSelectedProduct(){
    return this.selectedProduct
  }

}
