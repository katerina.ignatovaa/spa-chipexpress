import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {IProduct} from "../../models/product";

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private closeSubject = new Subject<IProduct[]>();
  readonly closeSubject$ = this.closeSubject.asObservable();

  constructor(private http: HttpClient) { }

  addCompany(company: {name: string}): Observable<any>{
    return this.http.post('http://localhost:3000/companies/', company);
  }

  getCompanies(): Observable<[{name: string}]>{
    return this.http.get<[{name: string}]>('http://localhost:3000/companies/');
  }

  closeModal(data: any) {
    this.closeSubject.next(data)
  }
}
