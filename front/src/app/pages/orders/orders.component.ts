import {Component, OnInit} from '@angular/core';
import {IOrder} from "../../models/order";
import {OrdersService} from "../../services/orders/orders.service";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit{

  orders: IOrder[];

  constructor(private ordersService: OrdersService) { }

  ngOnInit() {
    this.ordersService.getOrdersfromDb().subscribe((data) => {
      this.orders = data;
    })
  }
}
