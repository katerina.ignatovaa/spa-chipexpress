import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductInfoRoutingModule } from './product-info-routing.module';
import { ProductInfoComponent } from './product-info.component';
import {CarouselModule} from 'primeng/carousel';
import {ButtonModule} from 'primeng/button';
import {MessageService} from "primeng/api";
import {ToastModule} from "primeng/toast";

@NgModule({
  declarations: [
    ProductInfoComponent
  ],
  imports: [
    CommonModule,
    ProductInfoRoutingModule,
    CarouselModule,
    ButtonModule,
    ToastModule
  ],
  providers: [MessageService]
})
export class ProductInfoModule { }
