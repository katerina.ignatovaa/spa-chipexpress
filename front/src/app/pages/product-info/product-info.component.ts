import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../services/product/product.service";
import {IProduct} from "../../models/product";
import {ActivatedRoute, Router} from "@angular/router";
import {OrdersService} from "../../services/orders/orders.service";
import {IOrderProduct} from "../../models/order";
import {MessageService} from "primeng/api";
import {UserService} from "../../services/user/user.service";

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.scss']
})
export class ProductInfoComponent implements OnInit{

  product: IProduct;
  sameCompanyProducts: IProduct[];
  count: number = 2;

  constructor(private productService: ProductService,
              private route: ActivatedRoute,
              private ordersService: OrdersService,
              private messageService: MessageService,
              private userService: UserService) {
  }

  ngOnInit() {
    const id = this.route.snapshot.queryParamMap.get('id');
    if(id){
      this.productService.getProductById(id).subscribe((data) => {
        this.product = data;
        this.productService.getProductsByCompany(this.product.company).subscribe((data) => {
          this.sameCompanyProducts = data
        })
        }
      )
    };
    if(window.innerWidth < 576){
      this.count = 1
    };
    if(window.innerWidth > 1200){
      this.count = 3
    }
  }

  addToCart(){
    const order: IOrderProduct = {
      img: this.product.img,
      productName: this.product.name,
      company: this.product.company,
      price: this.product.price,
    }
    const user = this.userService.getUser();
    this.ordersService.addOrder(order, user);
    this.messageService.add({severity: 'success', summary: 'Товар добавлен в корзину'});
  }

  goToProduct(product: IProduct){
    if(product._id){
      this.productService.getProductById(product._id).subscribe((data) => {
        this.product = data;
      })
    }
  }
}

