import { Component } from '@angular/core';
import {IUser} from "../../../models/user";
import {MessageService} from "primeng/api";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {IServerError} from "../../../models/server-error";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent {

  email: string;
  password: string;
  passwordConfirm: string;

  constructor(private messageService: MessageService,
              private http: HttpClient) {
  }

  register() {
    const user: IUser = {
      email: this.email,
      password: this.password
    }

    if (this.password !== this.passwordConfirm) {
      this.messageService.add({severity: 'error', summary: "Пароли не совпадают"});
    } else {
      this.http.post<IUser>('http://localhost:3000/users/', user).subscribe((data) => {
          this.messageService.add({severity: 'success', summary: "Регистрация прошла успешно"})
        },
        (error: HttpErrorResponse) => {
          const serverError = <IServerError>error.error;
          this.messageService.add({severity: 'warning', summary: serverError.errorText});
        }
      )
    }
  }
}
