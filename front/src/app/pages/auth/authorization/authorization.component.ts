import { Component } from '@angular/core';
import {MessageService} from "primeng/api";
import {IUser} from "../../../models/user";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {UserService} from "../../../services/user/user.service";
import {Router} from "@angular/router";
import {IServerError} from "../../../models/server-error";

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.scss']
})
export class AuthorizationComponent {

  email: string;
  password: string;
  isSelected: boolean;
  key: string = '';

  constructor(private messageService: MessageService,
              private http: HttpClient,
              private userService: UserService,
              private router: Router) {}

  login(){
    const user: IUser = {
      email: this.email,
      password: this.password,
      key: this.key
    }

    this.http.post<{access_token: string, isUserAdmin: boolean}>('http://localhost:3000/users/'+ user.email, user).subscribe((data) => {
      this.userService.setUser(user);
      const token: string = data.access_token;
      this.userService.setToken(token);
      this.userService.setAdmin(data.isUserAdmin);
      this.router.navigate(['products/products-list']);
    },
      (error: HttpErrorResponse) => {
        const serverError = <IServerError>error.error;
        this.messageService.add({severity: 'error', summary: serverError.errorText});
      }
    )
  }
}
