import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {IUser} from "../../../models/user";
import {UserService} from "../../../services/user/user.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {IServerError} from "../../../models/server-error";
import {MessageService} from "primeng/api";
import {OrdersService} from "../../../services/orders/orders.service";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit{

  oldPassword: string;
  newPassword: string;
  newPasswordConfirm: string;
  user: IUser;
  showOrder: boolean = false;
  @Output() closeModalEvent = new EventEmitter();

  constructor(private userService: UserService,
              private ordersService: OrdersService,
              private messageService: MessageService,
              private http: HttpClient) {
  }

  ngOnInit() {
    this.user = this.userService.getUser();
      this.ordersService.getOrderForUser().subscribe((data) => {
        if(data.length !== 0){
          this.showOrder = true;
        }
      })
    }

  changePassword(){
    const user: IUser = {
      email: this.user ? this.user.email : '',
      password: this.newPassword
    }
    if (this.newPassword !== this.newPasswordConfirm) {
      this.messageService.add({severity: 'error', summary: "Пароли не совпадают"});
    } else {
      this.http.put<IUser>('http://localhost:3000/users/'+ this.oldPassword, user).subscribe((data) => {
        this.closeModalEvent.emit();
        this.messageService.add({severity: 'success', summary: "Пароль успешно изменен"})
        },
        (error: HttpErrorResponse) => {
          const serverError = <IServerError>error.error;
          this.messageService.add({severity: 'error', summary: serverError.errorText});
        }
      )
    }
  }
}
