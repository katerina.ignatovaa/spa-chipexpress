import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account.component';
import {AccountRoutingModule} from "./account-routing.module";
import { ChangePasswordComponent } from './change-password/change-password.component';
import { InputTextModule} from 'primeng/inputtext';
import {FormsModule} from "@angular/forms";
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import {ButtonModule} from 'primeng/button';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import {DialogModule} from 'primeng/dialog';

@NgModule({
  declarations: [
    AccountComponent,
    ChangePasswordComponent,
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    InputTextModule,
    FormsModule,
    ToastModule,
    ButtonModule,
    ConfirmDialogModule,
    DialogModule
  ],
  providers: [MessageService, ConfirmationService]
})
export class AccountModule { }
