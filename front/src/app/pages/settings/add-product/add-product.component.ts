import {AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ICompany} from "../../../models/company";
import {ProductService} from "../../../services/product/product.service";
import {CompanyService} from "../../../services/company/company.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit{

  productForm: FormGroup;
  name: string;
  description: string;
  price: string;
  companies: ICompany[] = [{label: 'Не выбран', value: 'none'}];

constructor(private productService: ProductService,
            private companyService: CompanyService,
            private messageService: MessageService) {}

  ngOnInit(){
    this.companyService.getCompanies().subscribe((data) => {
      data.forEach((el) => {
        this.companies.push({label: el.name, value: el.name})
      });
    });

    this.productForm = new FormGroup({
      name: new FormControl('', Validators.required),
      company: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required),
      img: new FormControl('')
    })
  }

  selectCompany(ev: { ev: Event, value: ICompany }){
    this.productForm.patchValue({
      company: ev.value.value
    });
  }

  selectFile(ev: any): void{
    if(ev.currentFiles.length > 0){
      const file = ev.currentFiles[0];
      this.productForm.patchValue({
        img: file
      });
    }
  }

  addProduct(){
    const productData = this.productForm.getRawValue();
    let formData = new FormData();
    for(let prop in productData){
      formData.append(prop, productData[prop])
    }
    this.productService.addProduct(formData).subscribe((data) => {
      this.productService.updateProductsList(data);
      this.messageService.add({severity: 'success', summary: "Компонент добавлен успешно"})
    })
  }
}
