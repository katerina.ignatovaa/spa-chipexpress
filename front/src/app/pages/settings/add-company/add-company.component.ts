import { Component } from '@angular/core';
import {CompanyService} from "../../../services/company/company.service";
import {ICompany} from "../../../models/company";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-add-company',
  templateUrl: './add-company.component.html',
  styleUrls: ['./add-company.component.scss']
})
export class AddCompanyComponent {

  newCompany: string;

  constructor(private companyService: CompanyService,
              private messageService: MessageService) {}

  addCompany(data: string){
    const company = {name: data}
    this.companyService.addCompany(company).subscribe((data) => {
      this.companyService.closeModal(data);
      this.messageService.add({severity: 'success', summary: "Производитель добавлен успешно"})
    })
  }
}
