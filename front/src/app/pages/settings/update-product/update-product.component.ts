import {AfterContentInit, AfterViewInit, Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ICompany} from "../../../models/company";
import {ProductService} from "../../../services/product/product.service";
import {CompanyService} from "../../../services/company/company.service";
import {IProduct} from "../../../models/product";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.scss']
})
export class UpdateProductComponent implements OnInit, AfterContentInit{

  productForm: FormGroup;
  name: string;
  description: string;
  price: string;
  companies: ICompany[] = [{label: 'Не выбран', value: 'none'}];
  selectedProduct: IProduct;

  constructor(private productService: ProductService,
              private companyService: CompanyService,
              private messageService: MessageService) {}

  ngOnInit() {
    this.companyService.getCompanies().subscribe((data) => {
      data.forEach((el) => {
        this.companies.push({label: el.name, value: el.name})
      });
    });

    this.productForm = new FormGroup({
      name: new FormControl('', Validators.required),
      company: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required),
      img: new FormControl(),
    });

    this.selectedProduct = this.productService.getSelectedProduct();
  }

  ngAfterContentInit() {
    this.productForm.controls["name"].setValue(this.selectedProduct.name);
    this.productForm.controls["description"].setValue(this.selectedProduct.description);
    this.productForm.controls["price"].setValue(this.selectedProduct.price);
  }


  selectCompany(ev: { ev: Event, value: ICompany }){
    this.productForm.patchValue({
      company: ev.value.value
    });
  }

  selectFile(ev: any): void{
    if(ev.currentFiles.length > 0){
      const file = ev.currentFiles[0];
      this.productForm.patchValue({
        img: file
      });
    }
  }

  updateProduct(){
    const productData = this.productForm.getRawValue();
    let formData = new FormData();
    for(let prop in productData){
      formData.append(prop, productData[prop])
    }
    this.productService.updateProduct(formData).subscribe((data) => {
      this.productService.updateProductsList(data);
      this.messageService.add({severity: 'success', summary: "Товар успешно обновлен"})
    })
  }

}
