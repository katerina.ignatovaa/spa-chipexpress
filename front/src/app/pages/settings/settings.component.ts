import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProductService} from "../../services/product/product.service";
import {IProduct} from "../../models/product";
import {Subscription} from "rxjs";
import {CompanyService} from "../../services/company/company.service";
import {ICompany} from "../../models/company";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy{

  products: IProduct[];
  addModal: boolean = false;
  private updateSub: Subscription;
  private closeSub: Subscription;
  updateModal: boolean = false;

  constructor(private productService: ProductService,
              private companyService: CompanyService,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.productService.getProducts().subscribe((data) => {
        this.products = data;
      }
    );
    this.updateSub = this.productService.productSubject$.subscribe((data) => {
      this.products = data;
      this.updateModal = false;
      this.addModal = false;
    });
    this.closeSub = this.companyService.closeSubject$.subscribe((data) => {
      this.addModal = false;
    });
  }

  ngOnDestroy() {
    this.updateSub.unsubscribe()
  }

  showAddModal() {
    this.addModal = true;
  }

  deleteProduct(product: IProduct){
    if(product._id){
      const id = product._id;
      this.productService.deleteProductById(id).subscribe((data) => {
        this.products = data;
        this.messageService.add({severity: 'error', summary: "Товар удален"})
      })
    }
  }

  showUpdateModal(product: IProduct){
    this.updateModal = true;
    if(product._id){
      ProductService.productIdForUpdate = product._id;
    }
    this.productService.setSelectedProduct(product)
  }
}
