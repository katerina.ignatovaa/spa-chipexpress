import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ButtonModule} from 'primeng/button';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import {DialogModule} from 'primeng/dialog';
import { InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {FileUploadModule} from 'primeng/fileupload';
import {TableModule} from 'primeng/table';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { DropdownModule } from 'primeng/dropdown';
import { TabViewModule } from 'primeng/tabview';
import { AddProductComponent } from './add-product/add-product.component';
import { AddCompanyComponent } from './add-company/add-company.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api'

@NgModule({
  declarations: [
    SettingsComponent,
    AddProductComponent,
    AddCompanyComponent,
    UpdateProductComponent
  ],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    ButtonModule,
    DialogModule,
    InputTextModule,
    InputTextareaModule,
    FileUploadModule,
    TableModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule,
    TabViewModule,
    ToastModule,
  ],
  providers: [MessageService]
})
export class SettingsModule { }
