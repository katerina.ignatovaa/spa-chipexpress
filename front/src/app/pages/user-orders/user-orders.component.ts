import { Component } from '@angular/core';
import {IOrder} from "../../models/order";
import {OrdersService} from "../../services/orders/orders.service";
import {ConfirmationService} from "primeng/api";

@Component({
  selector: 'app-user-orders',
  templateUrl: './user-orders.component.html',
  styleUrls: ['./user-orders.component.scss']
})
export class UserOrdersComponent {

  orders: IOrder[];

  constructor(private ordersService: OrdersService,
              private confirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.ordersService.getOrderForUser().subscribe((data) => {
      this.orders = data;
    })
  }

  cancelOrder(order: IOrder){
    this.confirmationService.confirm({
      message: 'Вы действительно хотите отменить заказ?',
      accept: () => {
        this.ordersService.deleteOrderFromDb(order).subscribe((data) => {
          this.orders = data;
        })
      }
    })
  }
}
