import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserOrdersRoutingModule } from './user-orders-routing.module';
import { UserOrdersComponent } from './user-orders.component';
import {ButtonModule} from 'primeng/button';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';

@NgModule({
  declarations: [
    UserOrdersComponent
  ],
  imports: [
    CommonModule,
    UserOrdersRoutingModule,
    ButtonModule,
    ConfirmDialogModule
  ],
  providers: [ConfirmationService]
})
export class UserOrdersModule { }
