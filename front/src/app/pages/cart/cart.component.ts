import {Component, OnInit} from '@angular/core';
import {OrdersService} from "../../services/orders/orders.service";
import {IOrder, IOrderProduct} from "../../models/order";
import {Router} from "@angular/router";
import {UserService} from "../../services/user/user.service";
import {IUser} from "../../models/user";
import {MessageService} from "primeng/api";
import {HttpErrorResponse} from "@angular/common/http";
import {ProductService} from "../../services/product/product.service";

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit{

  orders: IOrderProduct[];
  ordersCopy: IOrderProduct[];
  totalPrice: number = 0;
  orderModal: boolean = false;
  userName: string;
  phone: string;
  user: IUser;
  orderNumber: string;
  successDialog: boolean = false;

  constructor(private ordersService: OrdersService,
              private router: Router,
              private userService: UserService,
              private messageService: MessageService,
              private productService: ProductService) {}

  ngOnInit() {
    this.user = this.userService.getUser();
    this.orders = this.ordersService.getOrders(this.user.email);
    this.ordersCopy = [...this.orders];
    this.orders.forEach((el) => {
      this.totalPrice += parseInt(el.price.split('$')[1])
    });
  }

  openProduct(name: string){
    this.productService.getProducts().subscribe((data) => {
      const product = data.filter((el) => el.name === name);
      if(product[0]){
        this.router.navigate([`/products/product-info`], {queryParams: {id: product[0]._id}})
      }
    })
  }

  deleteOrder(index: number){
    this.totalPrice -= parseInt(this.orders[index].price.split('$')[1])
    this.ordersCopy.splice(index,1);
    this.orders = this.ordersCopy;
    this.ordersCopy = [...this.orders];
    this.ordersService.deleteOrder(index, this.user.email);
  }

  sendOrder(){
    this.orderNumber = Date.now().toString();
    const productArr = this.orders.map((el) => el.productName)
    const order: IOrder = {
      productName: productArr.join(', '),
      price: '$' + this.totalPrice,
      userName: this.userName,
      email: this.user ? this.user.email : '',
      phone: this.phone,
      date: new Date().toLocaleString('ru-RU', { year: 'numeric', month: 'numeric', day: 'numeric' }),
      orderNumber: this.orderNumber
    }

    this.ordersService.sendOrder(order).subscribe((data) => {
      this.orderModal = false;
      this.orders = [];
      this.ordersCopy = [];
      this.ordersService.deleteAll(this.user.email);
      this.totalPrice = 0;
      this.successDialog = true
    },
      (error: HttpErrorResponse) => {
        this.orderModal = false;
        this.messageService.add({severity: 'error', summary: 'Пользователь не авторизован'});
        }
      )
  }
}
