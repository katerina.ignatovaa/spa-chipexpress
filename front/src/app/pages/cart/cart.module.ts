import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartRoutingModule } from './cart-routing.module';
import { CartComponent } from './cart.component';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import { InputTextModule} from 'primeng/inputtext';
import {FormsModule} from "@angular/forms";
import {MessageService} from "primeng/api";
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [
    CartComponent
  ],
  imports: [
    CommonModule,
    CartRoutingModule,
    ButtonModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    ToastModule
  ],
  providers: [MessageService]
})
export class CartModule { }
