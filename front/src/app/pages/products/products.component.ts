import {Component, OnDestroy} from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnDestroy{

ngOnDestroy() {
  window.localStorage.removeItem('user');
  window.localStorage.removeItem('token');
  window.localStorage.removeItem('admin')
}

}
