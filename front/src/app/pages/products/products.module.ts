import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownModule } from 'primeng/dropdown';
import { ProductsRoutingModule } from './products-routing.module';
import { ProductsComponent } from './products.component';
import { HeaderComponent } from './header/header.component';
import {FooterComponent} from "./footer/footer.component";
import { ProductsListComponent } from './products-list/products-list.component';
import { InputTextModule } from "primeng/inputtext";
import { FormsModule } from "@angular/forms";
import {PaginatorModule} from 'primeng/paginator';
import {ButtonModule} from 'primeng/button';
import {MessageService} from "primeng/api";
import { ToastModule } from 'primeng/toast';

@NgModule({
  declarations: [
    ProductsComponent,
    HeaderComponent,
    FooterComponent,
    ProductsListComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    InputTextModule,
    FormsModule,
    DropdownModule,
    PaginatorModule,
    ButtonModule,
    ToastModule
  ],
  providers: [MessageService]
})
export class ProductsModule { }
