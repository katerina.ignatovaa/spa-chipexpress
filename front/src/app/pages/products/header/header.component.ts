import {Component, OnInit} from '@angular/core';
import {UserService} from "../../../services/user/user.service";
import {window} from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{

  isUserAdmin: boolean;
  windowWidth: number;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.isUserAdmin = this.userService.getAdmin();
    this.windowWidth = document.body.clientWidth;
  }

}
