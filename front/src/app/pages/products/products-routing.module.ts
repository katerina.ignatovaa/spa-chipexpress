import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProductsComponent} from "./products.component";
import {ProductsListComponent} from "./products-list/products-list.component";

const routes: Routes = [{
  path: '', component: ProductsComponent,
  children: [{
    path: 'products-list',
    component: ProductsListComponent
  },
    {
      path: 'product-info',
      loadChildren: ()  => import('../product-info/product-info.module').then(m => m.ProductInfoModule)
    },
    {
      path: 'cart',
      loadChildren: ()  => import('../cart/cart.module').then(m => m.CartModule)
    },
    {
      path: 'account',
      loadChildren: ()  => import('../account/account.module').then(m => m.AccountModule)
    },
    {
      path: 'settings',
      loadChildren: ()  => import('../settings/settings.module').then(m => m.SettingsModule)
    },
    {
      path: 'orders',
      loadChildren: ()  => import('../orders/orders.module')
        .then(m => m.OrdersModule)
    },
    {
      path: 'account/user-orders',
      loadChildren: ()  => import('../user-orders/user-orders.module').then(m => m.UserOrdersModule)
    }
 ]
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
