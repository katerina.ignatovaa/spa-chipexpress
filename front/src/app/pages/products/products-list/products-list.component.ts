import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IProduct} from "../../../models/product";
import {catchError, debounceTime, fromEvent, of, Subject, Subscription} from "rxjs";
import {ProductService} from "../../../services/product/product.service";
import {ICompany} from "../../../models/company";
import {Router} from "@angular/router";
import {CompanyService} from "../../../services/company/company.service";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements AfterViewInit, OnInit, OnDestroy{

  productSearchValue: string;
  @ViewChild('productSearch') productSearch: ElementRef;
  searchSub: Subscription;
  products: IProduct[];
  productsCopy: IProduct[];
  private subject = new Subject<ICompany>();
  selectSub: Subscription;
  companies: ICompany[] = [{label: 'Все производители', value: 'all'}];
  countOfShownProducts: number = 10;
  shownProducts: IProduct[];

  constructor(private productService: ProductService,
              private router: Router,
              private companyService: CompanyService,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.productService.getProducts()
      .pipe(
        catchError(err => {
          this.messageService.add({severity: 'error', summary: 'Сервер не отвечает'});
          return of([])
        })
      )
      .subscribe((data) => {
      this.products = data;
      this.productsCopy = [...this.products];
      this.shownProducts = this.products.slice(0, this.countOfShownProducts);
      }
    );
    this.companyService.getCompanies().subscribe((data) => {
      data.forEach((el) => {
        this.companies.push({label: el.name, value: el.name})
      });
    });
   }

  ngAfterViewInit() {
    this.searchSub = fromEvent(this.productSearch.nativeElement, 'keyup')
      .pipe(debounceTime(200))
      .subscribe((ev) => {
          if(this.productSearchValue){
            this.products = this.productsCopy.filter(
              (el) => el.name.toLowerCase().includes(this.productSearchValue.toLowerCase())
            )
          } else {
            this.products = [...this.productsCopy];
          }
          this.shownProducts = this.products.slice(0, this.countOfShownProducts);
        }
      )

    this.selectSub = this.subject.subscribe((data) => {
        if (data.value !== 'all') {
          this.productService.getProductsByCompany(data.value).subscribe((data) => {
            this.products = data;
            this.shownProducts = this.products.slice(0, this.countOfShownProducts);
          })
        } else {
          this.products = [...this.productsCopy];
          this.shownProducts = this.products.slice(0, this.countOfShownProducts);
        }
    })
  }

  ngOnDestroy(){
    this.searchSub.unsubscribe();
    this.selectSub.unsubscribe()
  }

  changeCompany(ev: { ev: Event, value: ICompany }){
    this.subject.next(ev.value)
  }

  openProduct(product: IProduct){
    this.router.navigate([`/products/product-info`], {queryParams: {id: product._id}})
  }

  scrollToTop(){
    window.scrollTo(0,0)
  }

  paginate(ev: { ev: Event, page: number }) {
    this.shownProducts = this.products.slice(this.countOfShownProducts * ev.page, this.countOfShownProducts * (ev.page + 1));
    window.scrollTo(0,0)
  }
}
